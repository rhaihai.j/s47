// Base Imports
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


// App Components
import AppNavBar from './components/AppNavBar';

// Page Components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';

const App = () =>{
    return(   
    <BrowserRouter>
    <AppNavBar/>
    <Routes>
        <Route path = "/courses" element={<Courses />} />        
        <Route path = "/register" element={<Register />} /> 
        <Route path ="/login" element = {<Login/>} />
        <Route path ="/" element = {<Home/>} />
        <Route path="*" element = {<NotFound />} />
    </Routes>
    </BrowserRouter>
    );
}

export default App;