import{ useState, useEffect } from 'react'

import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'



const Login = () =>{
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isDisabled, setIsDisabled] = useState(true)
    useEffect(()=>{
        let emailisNotEmpty = email !== '';
        let passwordisNotEmpty = password !== '';

        if(emailisNotEmpty && passwordisNotEmpty){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }

    },[email, password])

    const mockLogin = (e) =>{
        e.preventDefault()
        if(email === '111@email.com' && password === '111'){
            alert('Loggin in...')
        }else{
            alert('Invalid Login Credentials')
        }
        
    }
    return(
        <Container>
            <Form onSubmit={mockLogin}>
                <Form.Group>
                <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value={email} onChange = {(e)=>{ setEmail(e.target.value) }}/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required required value={password} onChange = {(e)=>{ setPassword(e.target.value) }} />
                    <Button variant="primary" type="submit" disabled={isDisabled} >Submit</Button>
            </Form.Group>
            </Form>
        </Container>
    )
}

export default Login;