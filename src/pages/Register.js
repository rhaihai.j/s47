import {useState, useEffect } from 'react'

import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'



const Register = () =>{

    const [email, setEmail]= useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [isDisabled, setIsDisabled]=useState(true)

    // useEffect(()=>{
    //     console.log(email)
    // },[email])


    useEffect(()=>{
        let isEmailisNotEmpty = email !== '';
        let isPasswordisNotEmpty = password !== '';
        let isPasswordConfirmisNotEmpty = passwordConfirm !== '';
        let isPasswordMatched = password === passwordConfirm;

        if(isEmailisNotEmpty && isPasswordisNotEmpty && isPasswordConfirmisNotEmpty &&isPasswordMatched){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }

    },[email, password, passwordConfirm])

    const register = (e) => {
        e.preventDefault()
        alert('Registration Pending...')
    }

    return(
        <Container>
            <h3>Register</h3>
            <Form onSubmit={register}>
            <Form.Group>
            <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value={email} onChange = {(e)=>{ setEmail(e.target.value) }}/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
            </Form.Group>
            <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value={password} onChange = {(e)=>{ setPassword(e.target.value) }}/>
            </Form.Group>
            <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" required value={passwordConfirm} onChange = {(e)=>{ setPasswordConfirm(e.target.value) }}/>
                    <Button variant="primary" type="submit" disabled={isDisabled} >Submit</Button>
            </Form.Group>
            </Form>
        </Container>
    );
}

export default Register