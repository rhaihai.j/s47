const NotFound = () =>{
    return(
        <>
        <h3>Page Not Found</h3>
        <p>Go Back to the <a href="/">homepage</a>.</p>
        </>
        
    );
}

export default NotFound;