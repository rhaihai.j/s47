const Welcome = (props) =>{
    return(
        <h1>Hello {props.name}, Your Age is {props.age}</h1>
    )   
}

export default Welcome;