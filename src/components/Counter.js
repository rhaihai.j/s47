import { useState, useEffect } from 'react'
import Container from 'react-bootstrap/Container'
import button from 'react-bootstrap/Button';

const Counter = () => {
    const [count, setCount] = useState(0)
    useEffect(()=>{
        document.title =`You Clicked ${count} times`
        console.log("render")
    },[count])

    return(
        <Container fluid>
                <p>You Clicked {count} times</p>
                <button onClick={()=>{
                    setCount(count + 1)
                }
            }>Click Me</button>
        </Container>
    )
}

export default Counter;